# frozen_string_literal: true

require 'fileutils'
require 'json'

RSpec.describe LsifParser::IO do
  let(:file_name) { 'dump.lsif' }
  let(:tmp_folder) { "#{file_name}.tmp" }
  let(:prefix) { '/Users/igordrozdov/Documents/Projects/go-project' }

  around do |example|
    example.run
  ensure
    FileUtils.rm_rf(tmp_folder)
  end

  context 'parse LSIF file' do
    before do
      described_class.process("spec/fixtures/#{file_name}", doc_prefix: prefix)
    end

    it 'is stored as a directory of JSON files' do
      main_go = JSON.parse(File.read("#{tmp_folder}/main.go.json"))
      reverse_go = JSON.parse(File.read("#{tmp_folder}/morestrings/reverse.go.json"))

      expect(main_go.first).to eq({
        'definition_path' => 'main.go#L4',
        'hover' => [
          {
            'language' => 'go',
            'value' => LsifParser::Highlight.run('language' => 'go', 'value' => "package \"github.com/user/hello/morestrings\"")
          },
          {
            'value' => "Package morestrings implements additional functions to manipulate UTF-8 encoded strings, beyond what is provided in the standard \"strings\" package. \n\n"
          }
        ],
        'start_line' => 7,
        'start_char' => 1
      })
      expect(reverse_go.first).to eq({
        'definition_path' => 'morestrings/reverse.go#L12',
        'hover' => [
          {
            'language' => 'go',
            'value' => LsifParser::Highlight.run('language' => 'go', 'value' => "func Reverse(s string) string")
          },
          {
            'value' => "This method reverses a string \n\n"
          }
        ],
        'start_line' => 11,
        'start_char' => 5
      })
    end
  end
end
