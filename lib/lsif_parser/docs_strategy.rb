# frozen_string_literal: true

require 'json'
require 'fileutils'
require 'lsif_parser/ranges_strategy'

module LsifParser
  # Creates a folder json files with code navigation data
  # The folder imitates the structure of the original folder
  class DocsStrategy
    def initialize(ranges_strategy: RangesStrategy.new)
      @ranges_strategy = ranges_strategy
    end

    def process(file_path, docs)
      docs.each do |id, path|
        ranges_for_file = @ranges_strategy.process(docs.doc_ranges[id], docs)

        _, source_file_name = File.split(file_path)
        file_dir, file_name = File.split(path)
        absolute_dir, = FileUtils.mkdir_p("#{source_file_name}.tmp/#{file_dir}")

        File.open([absolute_dir, "#{file_name}.json"].join('/'), 'w') do |f|
          f.write(JSON.dump(ranges_for_file))
        end
      end
    end
  end
end
