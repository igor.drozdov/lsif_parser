# frozen_string_literal: true

RSpec.describe LsifParser::Hovers do
  let(:data_line) do
    { 'id' => 2, 'label' => 'hoverResult', 'result' => { 'contents' => 'func hello' }}
  end
  let(:ref_line) do
    { 'id' => 3, 'label' => 'textDocument/references', 'outV' => 3, 'inV' => 1 }
  end
  let(:hover_line) do
    { 'id' => 4, 'label' => 'textDocument/hover', 'outV' => 3, 'inV' => 2 }
  end

  it 'returns hover content for a range id' do
    subject.read(data_line)
    subject.read(hover_line)
    subject.read(ref_line)

    expect(subject.for(1)).to eq('func hello')
  end
end
