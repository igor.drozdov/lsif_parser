# frozen_string_literal: true

module LsifParser
  # Processes and stores documents data and links to their ranges
  class Docs
    PREFIX = 'file://'

    attr_reader :docs, :doc_ranges, :ranges

    def initialize(prefix)
      @prefix = prefix.delete_prefix('/')
      @docs = {}
      @doc_ranges = {}
      @ranges = Ranges.new
    end

    def read(line)
      case line['label']
      when 'document'
        add(line)
      when 'contains'
        add_ranges(line)
      else
        @ranges.read(line)
      end
    end

    def each
      docs.each do |id, path|
        next if path.start_with?(PREFIX)

        yield id, path
      end
    end

    def find(id)
      @docs[id]
    end

    private

    def add(line)
      id = line['id']
      url = line['uri']

      docs[id] =
        url
        .delete_prefix("#{PREFIX}/#{prefix}")
        .delete_prefix('/')
    end

    def add_ranges(line)
      @doc_ranges[line['outV']] = line['inVs']
    end

    attr_reader :prefix
  end
end
