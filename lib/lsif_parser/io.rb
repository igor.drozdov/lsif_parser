# frozen_string_literal: true

require 'json'
require 'lsif_parser/docs_strategy'

module LsifParser
  # Processes an LSIF file
  class IO
    def self.process(path, doc_prefix: '', docs_strategy: DocsStrategy.new)
      docs = Docs.new(doc_prefix)

      File.open(path, 'r').each_line do |raw_line|
        line = JSON.parse(raw_line)

        docs.read(line)
      end

      docs_strategy.process(path, docs)
    end
  end
end
