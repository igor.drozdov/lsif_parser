# frozen_string_literal: true

require 'rouge'

module LsifParser
  # Highlight hovers
  class Highlight
    # HTML formatter for a hover
    class HTMLGitlab < Rouge::Formatters::HTML
      tag 'html_gitlab'

      def initialize(tag: nil)
        @line_number = 1
        @tag = tag
      end

      def stream(tokens)
        is_first = true
        token_lines(tokens) do |line|
          yield "\n" unless is_first
          is_first = false

          yield %(<span id="LC#{@line_number}" class="line" lang="#{@tag}">)
          line.each { |token, value| yield span(token, value.chomp) }
          yield %(</span>)

          @line_number += 1
        end
      end
    end

    def self.run(hover)
      lexer = Rouge::Lexer.find_fancy(hover['language'])
      tag = lexer.tag
      tokens = lexer.lex(hover['value'], continue: false)
      HTMLGitlab.format(tokens, tag: tag)
    end
  end
end
