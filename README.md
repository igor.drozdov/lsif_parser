# LsifParser

A library for parsing LSIF files using Ruby

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'lsif_parser'
```

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install lsif_parser

## Usage

Examples of using a particular class of the library can be found in the tests
(`spec` folder)

The basic usage with default strategy (and the one exposed by the binary)
processes an LSIF file and creates a folder with JSON files which imitate the
structure of the original project:

Project structure:

```
app
  controllers
    application_controller.rb
  models
    application.rb
  root.rb
```

Code navigation data structure:

```
app
  controllers
    application_controller.rb.json
  models
    application.rb.json
  root.rb.json
```

```ruby
LsifParser::IO.process('dump.lsif', doc_prefix: '/Users/igordrozdov/Documents/Projects/project')
```

## Development

After checking out the repo, run `bin/setup` to install dependencies. Then, run `rake spec` to run the tests. You can also run `bin/console` for an interactive prompt that will allow you to experiment.

To install this gem onto your local machine, run `bundle exec rake install`. To release a new version, update the version number in `version.rb`, and then run `bundle exec rake release`, which will create a git tag for the version, push git commits and tags, and push the `.gem` file to [rubygems.org](https://rubygems.org).

## Contributing

Bug reports and merge requests are welcome on GitLab at https://gitlab.com/igor-drozdov/lsif_parser.

## License

The gem is available as open source under the terms of the [MIT License](https://opensource.org/licenses/MIT).
